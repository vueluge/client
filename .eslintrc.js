module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    'vue/setup-compiler-macros': true,
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-essential',
    '@vue/standard',
    '@vue/typescript/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:import/typescript',
  ],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  plugins: [
    '@typescript-eslint',
    'modules-newline',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-useless-constructor': 'off',
    'object-curly-newline': [
      'error',
      {
        ImportDeclaration: {multiline: true, minProperties: 2},
        ExportDeclaration: {multiline: true, minProperties: 2},
      },
    ],
    'object-curly-spacing': ['error', 'never'],
    'require-await': 0,
    camelcase: 'off',
    quotes: ['error', 'single'],

    // TS overrides
    'comma-dangle': 'off',
    '@typescript-eslint/comma-dangle': ['error', 'always-multiline'],

    semi: 'off',
    '@typescript-eslint/semi': ['error'],

    indent: 'off',
    '@typescript-eslint/indent': ['error', 2],

    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],

    '@typescript-eslint/member-delimiter-style': ['error'],
    '@typescript-eslint/no-var-requires': 0,

    'space-before-function-paren': 'off',
    '@typescript-eslint/space-before-function-paren': ['error'],

    'func-call-spacing': 'off',
    '@typescript-eslint/func-call-spacing': ['error'],

    // Imports
    'modules-newline/import-declaration-newline': 'warn',
    'modules-newline/export-declaration-newline': 'warn',

    'import/export': ['off'],
    'import/no-unassigned-import': [
      'off',
      {
        // allow: [
        //   '**/registration',
        //   '~/plugins/**',
        // ],
      },
    ],
    'import/order': [
      'error',
      {
        groups: [
          ['builtin', 'external'],
          ['internal'],
        ],
        pathGroups: [
          {
            pattern: '#/**',
            group: 'internal',
          },
          {
            pattern: '~/**',
            group: 'internal',
          },
        ],
        pathGroupsExcludedImportTypes: ['builtin'],
        'newlines-between': 'always',
        alphabetize: {
          order: 'asc',
          caseInsensitive: false,
        },
      },
    ],
  },
  globals: {
    initSqlJs: 'readonly',
  },
};
