/// <reference types="vite/client" />

type Definite<T> = Exclude<T, undefined | null>;
type Optional<T> = T | undefined | null;
type Partially<T> = { [P in keyof T]?: Optional<T[P]>; };
