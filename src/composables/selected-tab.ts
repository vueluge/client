import {storeToRefs} from 'pinia';
import {
  nextTick,
  onMounted,
  onUnmounted,
  ref,
  watch,
  type WatchStopHandle,
} from 'vue';

import {
  VuelugeTorrentDetailsTab,
  type CadenceHook,
} from '~/lib';
import {
  useCadenceStore,
  useSelectedTorrentStore,
  useTorrentStore,
} from '~/stores';

const FOOTER_TAB_HOOK = 'FOOTER_TAB_HOOK';

export const useSelectedTab = () => {
  const cadenceStore = useCadenceStore();
  const torrentStore = useTorrentStore();
  const selectedTorrentStore = useSelectedTorrentStore();
  const {selectedId} = storeToRefs(selectedTorrentStore);
  const selectedTab = ref<VuelugeTorrentDetailsTab>(VuelugeTorrentDetailsTab.STATUS);
  const addHookAndCall = (hook: CadenceHook) => {
    cadenceStore.addHook(FOOTER_TAB_HOOK, hook);
    nextTick(hook);
  };
  let stopWatching: WatchStopHandle;

  onMounted(() => {
    stopWatching = watch([selectedTab, selectedId], ([tab, id]) => {
      if (id) {
        switch (tab) {
          case VuelugeTorrentDetailsTab.STATUS:
          case VuelugeTorrentDetailsTab.DETAILS:
            addHookAndCall(() => torrentStore.getDetails(id));
            break;

          case VuelugeTorrentDetailsTab.FILES:
            addHookAndCall(() => torrentStore.getFiles(id));
            break;

          case VuelugeTorrentDetailsTab.PEERS:
            addHookAndCall(() => torrentStore.getPeers(id));
            break;

          case VuelugeTorrentDetailsTab.OPTIONS:
            addHookAndCall(() => torrentStore.getOptions(id));
            break;

          default:
            break;
        }
      }
    });
  });
  onUnmounted(() => {
    cadenceStore.removeHook(FOOTER_TAB_HOOK);
    stopWatching?.();
  });

  return {
    selectedTab,
  };
};
