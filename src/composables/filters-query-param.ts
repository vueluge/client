import {
  useRoute,
  useRouter,
} from 'vue-router';

import type {
  VuelugeTorrentFilterId,
  VuelugeTorrentFilters,
} from '~/lib/filters';
import {vuelugeFilterRoutingGetFromQp} from '~/lib/filters/routing';

export const useFiltersQueryParam = (qp = 'filters') => {
  const route = useRoute();
  const router = useRouter();
  const select = (group: keyof VuelugeTorrentFilters, filter: VuelugeTorrentFilterId) => {
    const filters = vuelugeFilterRoutingGetFromQp(route, qp);
    filters[group] = filter;
    router.push({name: route.name as string,
      query: {
        ...route.query,
        [qp]: btoa(JSON.stringify(filters)),
      }});
  };

  return {
    select,
  };
};
