import {useRoute} from 'vue-router';

import {
  vuelugeRouterQueryParamsGet,
  vuelugeRouterQueryParamsGetAll,
} from '~/lib/router';

export interface RouteQueryParams {
  get(name: string): string;
  getAll(name: string): string[];
}

export const useRouteQueryParams = (): RouteQueryParams => {
  const route = useRoute();
  const get = (name: string): string => vuelugeRouterQueryParamsGet(route.query, name);
  const getAll = (name: string): string[] => vuelugeRouterQueryParamsGetAll(route.query, name);

  return {
    get,
    getAll,
  };
};
