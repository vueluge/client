import {storeToRefs} from 'pinia';
import {
  computed,
  ref,
} from 'vue';

import type {VuelugeAddTorrentPreset} from '~/lib';
import {usePluginConfigStore} from '~/stores';

export const useAddTorrentPresets = () => {
  const pluginConfigStore = usePluginConfigStore();
  const {config} = storeToRefs(pluginConfigStore);
  const list = computed(() => Object.values(config.value.presets));
  const _applied = ref<VuelugeAddTorrentPreset['name'] | null>(null);
  const lookup = (name: VuelugeAddTorrentPreset['name']) => config.value.presets[name];
  const apply = (name: VuelugeAddTorrentPreset['name']) => {
    const preset = lookup(name);

    if (preset) {
      _applied.value = name;
    }
  };
  const applied = computed<VuelugeAddTorrentPreset['name'] | null>({
    get: () => _applied.value,
    set: (val) => {
      if (val) {
        apply(val);
      }
    },
  });

  return {
    list,
    applied,
    apply,
    lookup,
  };
};
