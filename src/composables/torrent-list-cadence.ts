import {useHead} from '@vueuse/head';
import {storeToRefs} from 'pinia';
import Container from 'typedi';
import {
  onMounted,
  onUnmounted,
} from 'vue';

import {TitleService} from '~/lib/title/service';
import {
  useTorrentStore,
  useCadenceStore,
  useStatsStore,
} from '~/stores';

const TORRENT_LIST_HOOK = 'TORRENT_LIST_HOOK';

export const useTorrentListCadence = async () => {
  const torrentStore = useTorrentStore();
  const {stats} = storeToRefs(useStatsStore());
  const cadenceStore = useCadenceStore();
  const list = torrentStore.list();
  const {hooks} = storeToRefs(cadenceStore);

  onMounted(() => {
    hooks.value[TORRENT_LIST_HOOK] = async () => {
      await torrentStore.list();
      useHead({
        title: Container.get(TitleService).getFromStats(stats.value.download_rate, stats.value.upload_rate),
      });
    };
  });
  onUnmounted(() => {
    delete hooks.value[TORRENT_LIST_HOOK];
  });

  await list;
};
