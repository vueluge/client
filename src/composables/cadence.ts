import {
  onMounted,
  onUnmounted,
} from 'vue';

import {useCadenceStore} from '~/stores';

export const useCadence = () => {
  const {start, stop} = useCadenceStore();
  onMounted(() => {
    start();
  });
  onUnmounted(() => {
    stop();
  });
};
