import {defineStore} from 'pinia';
import Container from 'typedi';
import {
  computed,
  ref,
} from 'vue';

import type {
  CadenceHook,
  CadenceHooks,
} from '~/lib';

const DEFAULT_CADENCE_INTERVAL = 1000;

export const useCadenceStore = defineStore('cadence', () => {
  const interval = ref(DEFAULT_CADENCE_INTERVAL);
  const hooks = ref<CadenceHooks>({});
  const cadence = ref<ReturnType<typeof setInterval> | null>(null);
  const runHooks = () => Object.values(hooks.value).forEach(hook => hook());
  const createCadence = (int: number) => setInterval(
    runHooks,
    int,
  );
  const isRunning = computed(() => !!cadence.value);
  const start = () => {
    if (!cadence.value) {
      cadence.value = createCadence(interval.value);
    }
  };
  const stop = () => {
    if (cadence.value) {
      clearInterval(cadence.value);
      Container.get(AbortController).abort();
      Container.set(AbortController, new AbortController());
      cadence.value = null;
    }
  };
  const setNewInterval = (newInterval: number) => {
    stop();
    interval.value = newInterval;
    start();
  };
  const addHook = (id: keyof CadenceHooks, hook: CadenceHook) => {
    hooks.value[id] = hook;
  };
  const removeHook = (id: keyof CadenceHooks) => {
    delete hooks.value[id];
  };
  const reset = () => {
    stop();
    runHooks();
    start();
  };

  return {
    interval,
    hooks,
    start,
    stop,
    reset,
    setNewInterval,
    isRunning,
    addHook,
    removeHook,
  };
});
