import {defineStore} from 'pinia';
import {ref} from 'vue';

import type {
  VuelugeSelectedFilters,
  VuelugeTorrentFilterId,
  VuelugeTorrentFilters,
} from '~/lib/filters';

export const useFiltersStore = defineStore('filters', () => {
  const filters = ref<VuelugeTorrentFilters>({});
  const selected = ref<VuelugeSelectedFilters>({});
  const select = (group: keyof VuelugeTorrentFilters, filter: VuelugeTorrentFilterId) => {
    selected.value[group] = filter;
  };

  return {
    filters,
    selected,
    select,
  };
});
