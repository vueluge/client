import {defineStore} from 'pinia';
import {ref} from 'vue';

import {
  useAuthCheckOperationStore,
  useAuthLoginOperationStore,
  useAuthLogoutOperationStore,
} from './operations';

export const useAuthStore = defineStore('auth', () => {
  const loginStore = useAuthLoginOperationStore();
  const checkStore = useAuthCheckOperationStore();
  const logoutStore = useAuthLogoutOperationStore();
  const loggedIn = ref<boolean>(false);

  const check = async () => {
    try {
      await checkStore.run();
      loggedIn.value = true;
    } catch {
      loggedIn.value = false;
    }

    return loggedIn.value;
  };

  const login = async (password: string) => {
    try {
      await loginStore.run(password);
      loggedIn.value = true;
    } catch {}
  };

  const logout = async () => {
    try {
      await logoutStore.run();
      loggedIn.value = false;
    } catch {}
  };

  return {
    check,
    login,
    logout,
    loggedIn,
  };
});
