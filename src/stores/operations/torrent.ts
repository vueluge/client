import Container from 'typedi';

import {defineOperationStore} from '~/lib';
import type {Torrent, TorrentOptions} from '~/lib/driver';
import {
  VUELUGE_TORRENT_DRIVER,
  type VuelugeTorrentDriverInterface,
} from '~/lib/driver/torrent';

let cache: any;

export const useTorrentDetailsOperationStore = <T extends Torrent = Torrent>(): ReturnType<ReturnType<typeof defineOperationStore<VuelugeTorrentDriverInterface<T>['getDetails']>>> => {
  if (!cache) {
    cache = defineOperationStore<VuelugeTorrentDriverInterface<T>['getDetails']>(
      'torrentDetailsOperation',
      (id: string) => Container.get<VuelugeTorrentDriverInterface<T>>(VUELUGE_TORRENT_DRIVER).getDetails(id),
    )();
  }

  return cache;
};

export const useTorrentFilesOperationStore = defineOperationStore<VuelugeTorrentDriverInterface['getFiles']>(
  'torrentFilesOperation',
  (id: string) => Container.get(VUELUGE_TORRENT_DRIVER).getFiles(id),
);

export const useTorrentPeersOperationStore = defineOperationStore<VuelugeTorrentDriverInterface['getPeers']>(
  'torrentPeersOperation',
  (id: string) => Container.get(VUELUGE_TORRENT_DRIVER).getPeers(id),
);

export const useTorrentOptionsOperationStore = defineOperationStore<VuelugeTorrentDriverInterface['getOptions']>(
  'torrentOptionsOperation',
  (id: string) => Container.get(VUELUGE_TORRENT_DRIVER).getOptions(id),
);

export const useTorrentSetOptionsOperationStore = defineOperationStore<VuelugeTorrentDriverInterface['setOptions']>(
  'torrentOptionsOperation',
  (id: string, options: Partial<TorrentOptions>) => Container.get(VUELUGE_TORRENT_DRIVER).setOptions(id, options),
);
