import Container from 'typedi';

import {defineOperationStore} from '~/lib';
import {
  VUELUGE_AUTH_DRIVER,
  type VuelugeAuthDriverInterface,
} from '~/lib/driver/auth';

export const useAuthLoginOperationStore = defineOperationStore<VuelugeAuthDriverInterface['login']>(
  'AuthLoginOperation',
  (password: string) => Container.get(VUELUGE_AUTH_DRIVER).login(password),
);

export const useAuthLogoutOperationStore = defineOperationStore<VuelugeAuthDriverInterface['logout']>(
  'AuthLogoutOperation',
  () => Container.get(VUELUGE_AUTH_DRIVER).logout(),
);

export const useAuthCheckOperationStore = defineOperationStore<VuelugeAuthDriverInterface['check']>(
  'AuthCheckOperation',
  () => Container.get(VUELUGE_AUTH_DRIVER).check(),
);
