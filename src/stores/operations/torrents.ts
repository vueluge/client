import Container from 'typedi';

import {defineOperationStore} from '~/lib';
import type {
  AddTorrentPayload,
  Torrent,
} from '~/lib/driver';
import {
  VUELUGE_TORRENTS_DRIVER,
  type VuelugeTorrentsDriverInterface,
} from '~/lib/driver/torrents';
import type {VuelugeSelectedFilters} from '~/lib/filters';
import type {VuelugeMetadataRequest} from '~/lib/metadata';

export const useTorrentListOperationStore = defineOperationStore<VuelugeTorrentsDriverInterface['list']>(
  'torrentListOperation',
  (additionalFields?: (keyof Torrent)[], filters?: VuelugeSelectedFilters, metadata?: VuelugeMetadataRequest) => Container.get(VUELUGE_TORRENTS_DRIVER).list(additionalFields, filters, metadata),
);

export const useTorrentAddOperationStore = defineOperationStore<VuelugeTorrentsDriverInterface['add']>(
  'torrentAddOperation',
  (payloads: AddTorrentPayload[]) => Container.get(VUELUGE_TORRENTS_DRIVER).add(payloads),
);

export const useTorrentConfigOperationStore = defineOperationStore<VuelugeTorrentsDriverInterface['getConfig']>(
  'torrentConfigOperation',
  () => Container.get(VUELUGE_TORRENTS_DRIVER).getConfig(),
);
