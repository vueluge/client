import Container from 'typedi';

import {defineOperationStore} from '~/lib';
import type {VuelugeConfig} from '~/lib/config';
import {
  VUELUGE_CONFIG_DRIVER,
  type VuelugeConfigDriverInterface,
} from '~/lib/config/driver';

export const usePluginConfigGetOperationStore = defineOperationStore<VuelugeConfigDriverInterface['get']>(
  'pluginConfigGetOperation',
  () => Container.get(VUELUGE_CONFIG_DRIVER).get(),
);

export const usePluginConfigSetOperationStore = defineOperationStore<VuelugeConfigDriverInterface['set']>(
  'pluginConfigSetOperation',
  (config: Partial<VuelugeConfig>) => Container.get(VUELUGE_CONFIG_DRIVER).set(config),
);
