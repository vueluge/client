import {defineStore} from 'pinia';
import {ref} from 'vue';

import type {VuelugeConfig} from '~/lib/config';

import {
  usePluginConfigGetOperationStore,
  usePluginConfigSetOperationStore,
} from './operations';

const DEFAULT_SETTINGS: VuelugeConfig = {
  presets: {},
  recentLocations: [],
};

export const usePluginConfigStore = defineStore('pluginConfig', () => {
  const getOperationStore = usePluginConfigGetOperationStore();
  const setOperationStore = usePluginConfigSetOperationStore();
  const config = ref<VuelugeConfig>({...DEFAULT_SETTINGS});

  const get = async () => {
    config.value = await getOperationStore.run();
  };
  const set = async (val: Partial<VuelugeConfig>) => {
    await setOperationStore.run(val);

    config.value = {
      ...config.value,
      ...val,
    };
  };

  return {
    config,
    get,
    set,
  };
});
