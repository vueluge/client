import {defineEntityStore} from '~/lib';
import type {TorrentPreview} from '~/lib/driver';

let cache: any;

export const useTorrentEntitiesStore = <T extends TorrentPreview = TorrentPreview>(): ReturnType<ReturnType<typeof defineEntityStore<T>>> => {
  if (!cache) {
    cache = defineEntityStore<T>('torrentEntities')();
  }
  return cache;
};
