import {defineStore} from 'pinia';
import {ref} from 'vue';

import type {DelugeSettings} from '~/lib/driver';

import {useTorrentConfigOperationStore} from './operations';

const DEFAULT_SETTINGS: DelugeSettings = {
  send_info: false,
  info_sent: 0,
  daemon_port: 58846,
  allow_remote: false,
  download_location: '/Downloads',
  listen_ports: [6881, 6891],
  random_port: true,
  outgoing_ports: [0, 0],
  random_outgoing_ports: true,
  listen_interface: '',
  copy_torrent_file: false,
  torrentfiles_location: '',
  del_copy_torrent_file: false,
  plugins_location: '',
  prioritize_first_last_pieces: false,
  dht: true,
  upnp: true,
  natpmp: true,
  utpex: true,
  lsd: true,
  enc_in_policy: 1,
  enc_out_policy: 1,
  enc_level: 2,
  enc_prefer_rc4: true,
  max_connections_global: 200,
  max_upload_speed: -1,
  max_download_speed: -1,
  max_upload_slots_global: 4,
  max_half_open_connections: 50,
  max_connections_per_second: 20,
  ignore_limits_on_local_network: true,
  max_connections_per_torrent: -1,
  max_upload_slots_per_torrent: -1,
  max_upload_speed_per_torrent: -1,
  max_download_speed_per_torrent: -1,
  enabled_plugins: [],
  autoadd_enable: false,
  add_paused: false,
  max_active_seeding: 5,
  max_active_downloading: 3,
  max_active_limit: 8,
  dont_count_slow_torrents: false,
  queue_new_to_top: false,
  stop_seed_at_ratio: false,
  remove_seed_at_ratio: false,
  stop_seed_ratio: 2,
  share_ratio_limit: 2,
  seed_time_ratio_limit: 7,
  seed_time_limit: 180,
  auto_managed: true,
  move_completed: false,
  move_completed_path: '',
  new_release_check: true,
  peer_tos: '0x00',
  rate_limit_ip_overhead: true,
  geoip_db_location: '/usr/share/GeoIP/GeoIP.dat',
  cache_size: 512,
  cache_expiry: 60,
};

export const useDelugeConfigStore = defineStore('delugeConfig', () => {
  const configOperationStore = useTorrentConfigOperationStore();
  const config = ref<DelugeSettings>({...DEFAULT_SETTINGS});

  const get = async () => {
    config.value = await configOperationStore.run();
  };

  return {
    config,
    get,
  };
});
