import {defineStore} from 'pinia';
import {ref} from 'vue';

import type {Stats} from '~/lib/driver';

export const useStatsStore = defineStore('stats', () => {
  const stats = ref<Stats>({
    upload_protocol_rate: 0,
    max_upload: 0,
    download_protocol_rate: 0,
    download_rate: 0,
    has_incoming_connections: false,
    num_connections: 0,
    max_download: 0,
    upload_rate: 0,
    dht_nodes: 0,
    free_space: 0,
    max_num_connections: 0,
  });

  return {
    stats,
  };
});
