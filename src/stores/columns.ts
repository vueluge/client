import {defineStore} from 'pinia';
import {
  computed,
  ref,
} from 'vue';

import {
  VUELUGE_DEFAULT_COLUMN_ORDER,
  VUELUGE_DEFAULT_SELECTED_COLUMNS,
  VUELUGE_TORRENT_COLUMNS,
  type VuelugeColumnId,
  type VuelugeSelectedColumns,
  type VuelugeTorrentColumn,
} from '~/lib';

export const useTorrentColumnsStore = defineStore('torrentColumns', () => {
  const columns = ref<VuelugeSelectedColumns>(VUELUGE_DEFAULT_SELECTED_COLUMNS);
  const order = ref(VUELUGE_DEFAULT_COLUMN_ORDER);
  const selectColumn = (id: VuelugeColumnId) => {
    columns.value[id] = true;
  };
  const deselectColumn = (id: VuelugeColumnId) => {
    columns.value[id] = false;
  };
  const move = (id: VuelugeColumnId, position: number) => {
    const index = order.value.indexOf(id);
    order.value.splice(position, 0, order.value.splice(index, 1)[0]);
  };
  const selectedIds = computed(() => order.value.filter(id => columns.value[id]));
  const selectedFields = computed(() => selectedIds.value.reduce<VuelugeTorrentColumn['fields']>(
    (acc, id) => {
      acc.push(...VUELUGE_TORRENT_COLUMNS[id].fields);
      return acc;
    },
    [],
  ));
  const selected = computed(() => selectedIds.value.map(id => VUELUGE_TORRENT_COLUMNS[id]));

  return {
    columns,
    order,
    selectedIds,
    selectedFields,
    selected,
    selectColumn,
    deselectColumn,
    move,
  };
});
