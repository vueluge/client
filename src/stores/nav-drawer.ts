import {defineStore} from 'pinia';
import {ref} from 'vue';

export const useNavDrawerStore = defineStore('navDrawer', () => {
  const opened = ref<boolean>(false);
  const open = () => {
    opened.value = true;
  };
  const close = () => {
    opened.value = false;
  };
  const toggle = () => {
    opened.value
      ? close()
      : open();
  };

  return {
    opened,
    open,
    close,
    toggle,
  };
});
