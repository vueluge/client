import {
  defineStore,
  storeToRefs,
} from 'pinia';
import {
  computed,
  ref,
} from 'vue';

import type {
  TorrentFiles,
  TorrentPeers,
  TorrentOptions,
  TorrentPreview,
  Torrent,
} from '~/lib/driver';

import {useTorrentColumnsStore} from './columns';
import {useFiltersStore} from './filters';
import {useMetadataStore} from './metadata';
import {
  useTorrentDetailsOperationStore,
  useTorrentFilesOperationStore,
  useTorrentListOperationStore,
  useTorrentOptionsOperationStore,
  useTorrentPeersOperationStore,
  useTorrentSetOptionsOperationStore,
} from './operations';
import {useStatsStore} from './stats';
import {useTorrentEntitiesStore} from './torrent-entities';

let cache: any;

const createStore = <T extends Torrent = Torrent>() => defineStore('torrent', () => {
  const listOperationStore = useTorrentListOperationStore();
  const detailsStore = useTorrentDetailsOperationStore<T>();
  const filesStore = useTorrentFilesOperationStore();
  const peersStore = useTorrentPeersOperationStore();
  const optionsStore = useTorrentOptionsOperationStore();
  const setOptionsStore = useTorrentSetOptionsOperationStore();
  const entities = useTorrentEntitiesStore<T>();
  const filtersStore = useFiltersStore();
  const statsStore = useStatsStore();
  const columnsStore = useTorrentColumnsStore();
  const metadataStore = useMetadataStore();

  const {filters, selected} = storeToRefs(filtersStore);
  const {stats} = storeToRefs(statsStore);
  const {selectedFields} = storeToRefs(columnsStore);
  const {request, response} = storeToRefs(metadataStore);

  const listIds = ref<TorrentPreview['id'][]>([]);
  const connected = ref(false);

  const files = ref<Record<TorrentPreview['id'], TorrentFiles['result']>>({});
  const peers = ref<Record<TorrentPreview['id'], TorrentPeers[]>>({});
  const options = ref<Record<TorrentPreview['id'], TorrentOptions>>({});

  const list = async () => {
    const data = await listOperationStore.run(selectedFields.value, selected.value, request.value);

    stats.value = data.stats;
    connected.value = data.connected;
    filters.value = {...data.filters};
    response.value = {
      pagination: data.pagination,
      sort: data.sort,
      count: data.count,
    };
    entities.upsert(...(<Array<T>>data.torrents));

    listIds.value = data.torrents.map(({id}) => id);
  };
  const getDetails = async (id: TorrentPreview['id']) => {
    const data = await detailsStore.run(id);

    entities.upsert(data);
  };
  const getFiles = async (id: TorrentPreview['id']) => {
    const data = await filesStore.run(id);

    files.value[id] = data;
  };
  const getPeers = async (id: TorrentPreview['id']) => {
    const data = await peersStore.run(id);

    peers.value[id] = data;
  };
  const getOptions = async (id: TorrentPreview['id']) => {
    const data = await optionsStore.run(id);

    options.value[id] = data;
  };
  const setOptions = async (id: TorrentPreview['id'], opts: Partial<TorrentOptions>) => {
    const data = await setOptionsStore.run(id, opts);

    options.value[id] = data;
  };

  const torrentList = computed(() => listIds.value.map(id => entities.get(id)));

  return {
    list,
    getDetails,
    getFiles,
    getPeers,
    getOptions,
    setOptions,
    stats,
    connected,
    filters,
    listIds,
    torrentList,
    options,
  };
});

export const useTorrentStore = <T extends Torrent = Torrent>(): ReturnType<ReturnType<typeof createStore<T>>> => {
  if (!cache) {
    cache = createStore<T>()();
  }

  return cache;
};
