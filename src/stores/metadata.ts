import {defineStore} from 'pinia';
import {ref} from 'vue';

import { VUELUGE_METADATA_REQUEST_DEFAULT, VUELUGE_METADATA_RESPONSE_DEFAULT, type VuelugeMetadataRequest, type VuelugeMetadataResponse } from '~/lib/metadata';

export const useMetadataStore = defineStore('metadata', () => {
  const request = ref<VuelugeMetadataRequest>(VUELUGE_METADATA_REQUEST_DEFAULT);
  const response = ref<VuelugeMetadataResponse>(VUELUGE_METADATA_RESPONSE_DEFAULT);

  return {
    request,
    response
  };
});
