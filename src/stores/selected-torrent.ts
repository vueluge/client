import {defineStore} from 'pinia';
import {
  ref,
  computed,
} from 'vue';

import type {TorrentPreview} from '~/lib/driver';

import {useTorrentEntitiesStore} from './torrent-entities';

export const useSelectedTorrentStore = defineStore('selectedTorrent', () => {
  const entities = useTorrentEntitiesStore();
  const selectedId = ref<TorrentPreview['id'] | null>(null);
  const selectedTorrent = computed(() => selectedId.value ? entities.get(selectedId.value) : null);
  const selectTorrent = (torrentId: TorrentPreview['id']) => {
    selectedId.value = torrentId;
  };

  return {
    selectedId,
    selectTorrent,
    selectedTorrent,
  };
});
