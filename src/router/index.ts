import {storeToRefs} from 'pinia';
import Container from 'typedi';
import {
  createRouter,
  createWebHashHistory,
} from 'vue-router';

import {
  VUELUGE_FILTER_ROUTING_CONFIG,
  vuelugeFilterRoutingGetFromQp,
} from '~/lib/filters/routing';
import {
  useAuthStore,
  useFiltersStore,
} from '~/stores';

import HomeView from '../views/HomeView.vue';
import LoginView from '../views/LoginView.vue';
import AppBar from '@/AppBar.vue';
import NavigationDrawer from '@/NavigationDrawer.vue';
import TorrentDetailsFooter from '@/TorrentDetailsFooter.vue';
import TorrentSystemBar from '@/TorrentSystemBar.vue';

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'root',
      redirect: (to) => {
        const {
          loggedIn,
        } = storeToRefs(useAuthStore());
        return loggedIn.value
          ? {
            name: 'home',
          }
          : {
            name: 'login',
          };
      },
    },
    {
      path: '/home',
      name: 'home',
      components: {
        default: HomeView,
        footer: TorrentDetailsFooter,
        header: AppBar,
        drawer: NavigationDrawer,
        systemBar: TorrentSystemBar,
      },
      async beforeEnter (to, from) {
        const {check} = useAuthStore();
        return await check() || {
          name: 'login',
          query: {
            redirect: to.fullPath,
          },
        };
      },

    },
    {
      path: '/login',
      name: 'login',
      components: {
        default: LoginView,
      },
      async beforeEnter (to, from) {
        const {check} = useAuthStore();

        return await check()
          ? {
            name: 'home',
          }
          : true;
      },
    },
  ],
});

// TODO: move to route guard, need route beforeUpdate
router.beforeEach((to, from) => {
  const filtersStore = useFiltersStore();
  const {
    selected,
  } = storeToRefs(filtersStore);
  const {queryParam: qpName} = Container.get(VUELUGE_FILTER_ROUTING_CONFIG);

  selected.value = vuelugeFilterRoutingGetFromQp(to, qpName);
});

export default router;
