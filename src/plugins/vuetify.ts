import 'vuetify/styles';
import {createVuetify} from 'vuetify';
import {
  aliases,
  mdi,
} from 'vuetify/iconsets/mdi-svg';
import * as labs from 'vuetify/labs/components';

export default createVuetify({
  components: {
    ...labs,
  },
  theme: {
    defaultTheme: 'dark',
  },
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: {
      mdi,
    },
  },
});
