export enum VuelugeTorrentDetailsTab {
  STATUS,
  DETAILS,
  FILES,
  PEERS,
  OPTIONS,
}
