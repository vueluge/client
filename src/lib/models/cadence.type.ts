export type CadenceHook = () => void;
export type CadenceHooks = Record<string, CadenceHook>;
