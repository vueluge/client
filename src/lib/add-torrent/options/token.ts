import {Token} from 'typedi';

import type {AddTorrentOptions} from '~/lib/driver';

export const VUELUGE_ADD_TORRENT_OPTIONS_DEFAULT = new Token<AddTorrentOptions>('VUELUGE_ADD_TORRENT_OPTIONS_DEFAULT');
