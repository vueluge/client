import {flatMap} from 'lodash';

import type {
  TorrentFSContents,
  TorrentContentFile,
} from '~/lib/driver';

export const vuelugeAddTorrentOptionsGetFiles = (contents: TorrentFSContents): TorrentContentFile[] =>
  flatMap(contents, (val) => val.type === 'dir' ? vuelugeAddTorrentOptionsGetFiles(val.contents) : [val]);
