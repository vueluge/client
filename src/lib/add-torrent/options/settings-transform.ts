import type {
  AddTorrentOptions,
  DelugeSettings,
} from '~/lib/driver';

export const vuelugeAddTorrentOptionsTransform = (settings: DelugeSettings): Partial<AddTorrentOptions> => ({
  download_location: settings.download_location,
  move_completed: settings.move_completed,
  move_completed_path: settings.move_completed_path,
  add_paused: settings.add_paused,
  prioritize_first_last_pieces: settings.prioritize_first_last_pieces,
  max_download_speed: settings.max_download_speed_per_torrent,
  max_upload_speed: settings.max_upload_speed_per_torrent,
  max_connections: settings.max_connections_per_torrent,
  max_upload_slots: settings.max_upload_slots_per_torrent,
});
