import type {VuelugeAddTorrentPreset} from './type';

export const VUELUGE_ADD_TORRENT_PRESET_DEFAULT: VuelugeAddTorrentPreset = {
  name: '',
  options: {
    add_paused: false,
    compact_allocation: false,
    max_connections: -1,
    max_download_speed: -1,
    max_upload_slots: -1,
    max_upload_speed: -1,
    prioritize_first_last_pieces: false,
    pre_allocate_storage: false,
    move_completed: false,
    seed_mode: false,
    sequential_download: false,
    super_seeding: false,
  },
};
