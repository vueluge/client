import type {AddTorrentOptions} from '~/lib/driver';

export interface VuelugeAddTorrentPreset {
  name: string;
  autoApplyRegex?: string;
  options: Partial<AddTorrentOptions>;
}
