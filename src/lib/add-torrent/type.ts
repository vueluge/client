import type {
  AddTorrentOptions,
  TorrentInfo,
} from '~/lib/driver';

export interface VuelugeAddTorrent {
  info: TorrentInfo['result'];
  options: AddTorrentOptions;
}
