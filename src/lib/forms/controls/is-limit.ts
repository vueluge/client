const NON_NEGATIVE_NUMBER_REGEX = /^\d+$/;

export const vuelugeFormsIsValidLimit = (value: string): boolean =>
  NON_NEGATIVE_NUMBER_REGEX.test(value) || Number(value) === -1;
