import {defineStore} from 'pinia';
import {ref} from 'vue';

type Operation = (...args: any[]) => Promise<any>;

export const defineOperationStore = <T extends Operation = Operation>(
  id: string,
  operation: T,
) =>
  defineStore(id, () => {
    const loading = ref(false);
    const errors = ref<string[]>([]);
    const beginOperation = () => {
      loading.value = true;
    };
    const operationSuccess = () => {
      loading.value = false;
      errors.value = [];
    };
    const operationFailure = (err: string) => {
      loading.value = false;
      errors.value.push(err);
    };
    const run = ((...args: Parameters<T>) => {
      beginOperation();
      return operation(...args).then(res => {
        operationSuccess();
        return res;
      }).catch((err: Error) => {
        operationFailure(err.message);
        throw err;
      });
    }) as T;

    return {
      loading,
      errors,
      run,
    };
  });
