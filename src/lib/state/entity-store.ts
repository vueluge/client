import {defineStore} from 'pinia';
import {
  computed,
  ref,
} from 'vue';

import {arrayToDict} from '../helpers';
import type {Identifiable} from '../models';

export type Entities<T extends Identifiable = Identifiable> = Record<T['id'], T>;

export const defineEntityStore = <T extends Identifiable = Identifiable>(id: string) =>
  defineStore(id, () => {
    // TODO: investigate batching changes to entities.value
    // better to build new entities dict and set?
    const entities = ref({} as Entities<T>);
    const add = (...items: T[]): void => {
      items.forEach(entity => {
        entities.value[entity.id] = entity;
      });
    };
    const update = (...items: T[]): void => {
      items.forEach(item => {
        const entity = entities.value[item.id];
        if (entity) {
          entities.value[entity.id] = {
            ...entity,
            ...item,
          };
        }
      });
    };
    const upsert = (...items: T[]): void => {
      items.forEach(item => {
        const entity = entities.value[item.id];
        if (entity) {
          update(item);
        } else {
          add(item);
        }
      });
    };
    const get = (entityId: T['id']): T => entities.value[entityId];
    const set = (items: T[]): void => {
      entities.value = arrayToDict(items);
    };
    const remove = (entityId: T['id']): void => {
      delete entities.value[entityId];
    };
    const list = computed<T[]>(() => Object.values(entities.value));
    const count = computed<number>(() => Object.keys(entities.value).length);

    return {
      add,
      get,
      update,
      upsert,
      set,
      list,
      remove,
      count,
    };
  });
