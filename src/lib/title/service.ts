import {Service} from 'typedi';

import {vuelugeFileSizeRateFormat} from '../formatters';

@Service()
export class TitleService {
  getFromStats (download: number, upload: number): string {
    return `D: ${vuelugeFileSizeRateFormat(download)} U: ${vuelugeFileSizeRateFormat(upload)} - Vueluge`;
  }

  setFromStats (download: number, upload: number): void {
    global.document.title = this.getFromStats(download, upload);
  }
}
