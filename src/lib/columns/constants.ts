
import type {Torrent} from '~/lib/driver';

import {
  vuelugeDurationFormat,
  vuelugeFileSizeFormat,
  vuelugeFileSizeRateFormat,
} from '../formatters';
import {VuelugeColumnId} from './enum';
import type {
  VuelugeSelectedColumns,
  VuelugeTorrentColumn,
  VuelugeTorrentColumns,
} from './type';

const queueColumn: VuelugeTorrentColumn<'queue', Torrent['queue']> = {
  id: VuelugeColumnId.QUEUE,
  label: '#',
  fields: ['queue'],
  format: queue => queue >= 0 ? String(queue) : '-',
};
const nameColumn: VuelugeTorrentColumn<'name', Torrent['name']> = {
  id: VuelugeColumnId.NAME,
  label: 'Name',
  fields: ['name'],
};
const sizeColumn: VuelugeTorrentColumn<'total_size', Torrent['total_size']> = {
  id: VuelugeColumnId.SIZE,
  label: 'Size',
  fields: ['total_size'],
  format: totalSize => vuelugeFileSizeFormat(totalSize),
};
const progressColumn: VuelugeTorrentColumn<'progress', Torrent['progress']> = {
  id: VuelugeColumnId.PROGRESS,
  label: 'Progress',
  fields: ['progress'],
  format: progress => `${progress.toFixed(2)}%`,
};
const seedsColumn: VuelugeTorrentColumn<'num_seeds' | 'total_seeds', [Torrent['num_seeds'], Torrent['total_seeds']]> = {
  id: VuelugeColumnId.SEEDS,
  label: 'Seeds',
  fields: ['num_seeds', 'total_seeds'],
  get: torrent => [torrent.num_seeds, torrent.total_seeds],
  format: ([numSeeds, totalSeeds]) => `${numSeeds} (${totalSeeds})`,
  compare: ([a], [b]) => a - b,
};
const peersColumn: VuelugeTorrentColumn<'num_peers' | 'total_peers', [Torrent['num_peers'], Torrent['total_peers']]> = {
  id: VuelugeColumnId.PEERS,
  label: 'Peers',
  fields: ['num_peers', 'total_peers'],
  get: torrent => [torrent.num_peers, torrent.total_peers],
  format: ([numPeers, totalPeers]) => `${numPeers} (${totalPeers})`,
  compare: ([a], [b]) => a - b,
};
const downSpeedColumn: VuelugeTorrentColumn<'download_payload_rate'> = {
  id: VuelugeColumnId.DOWNSPEED,
  label: 'Down Speed',
  fields: ['download_payload_rate'],
  format: rate => vuelugeFileSizeRateFormat(rate),
};
const upSpeedColumn: VuelugeTorrentColumn<'upload_payload_rate'> = {
  id: VuelugeColumnId.UPSPEED,
  label: 'Up Speed',
  fields: ['upload_payload_rate'],
  format: rate => vuelugeFileSizeRateFormat(rate),
};
const etaColumn: VuelugeTorrentColumn<'eta'> = {
  id: VuelugeColumnId.ETA,
  label: 'ETA',
  fields: ['eta'],
  format: eta => vuelugeDurationFormat(eta * 10e2, {compact: false}),
};
const ratioColumn: VuelugeTorrentColumn<'ratio'> = {
  id: VuelugeColumnId.RATIO,
  label: 'Ratio',
  fields: ['ratio'],
};
const availColumn: VuelugeTorrentColumn<'distributed_copies'> = {
  id: VuelugeColumnId.AVAIL,
  label: 'Avail',
  fields: ['distributed_copies'],
};
const addedColumn: VuelugeTorrentColumn<'time_added'> = {
  id: VuelugeColumnId.ADDED,
  label: 'Added',
  fields: ['time_added'],
};
const completeSeenColumn: VuelugeTorrentColumn<'last_seen_complete'> = {
  id: VuelugeColumnId.COMPLETESEEN,
  label: 'Complete Seen',
  fields: ['last_seen_complete'],
};
const completedColumn: VuelugeTorrentColumn<'completed_time'> = {
  id: VuelugeColumnId.COMPLETED,
  label: 'Completed',
  fields: ['completed_time'],
};
const trackerColumn: VuelugeTorrentColumn<'tracker_host'> = {
  id: VuelugeColumnId.TRACKER,
  label: 'Tracker',
  fields: ['tracker_host'],
};
const downloadFolderColumn: VuelugeTorrentColumn<'download_location'> = {
  id: VuelugeColumnId.DOWNLOADFOLDER,
  label: 'Download Folder',
  fields: ['download_location'],
};
const downloadedColumn: VuelugeTorrentColumn<'total_done'> = {
  id: VuelugeColumnId.DOWNLOADED,
  label: 'Downloaded',
  fields: ['total_done'],
  format: done => vuelugeFileSizeFormat(done),
};
const uploadedColumn: VuelugeTorrentColumn<'total_uploaded'> = {
  id: VuelugeColumnId.UPLOADED,
  label: 'Uploaded',
  fields: ['total_uploaded'],
  format: uploaded => vuelugeFileSizeFormat(uploaded),
};
const remainingColumn: VuelugeTorrentColumn<'total_remaining'> = {
  id: VuelugeColumnId.REMAINING,
  label: 'Remaining',
  fields: ['total_remaining'],
  format: total_remaining => vuelugeFileSizeFormat(total_remaining),
};
const downLimitColumn: VuelugeTorrentColumn<'max_download_speed'> = {
  id: VuelugeColumnId.DOWNLIMIT,
  label: 'Down Limit',
  fields: ['max_download_speed'],
  format: limit => vuelugeFileSizeRateFormat(limit),
};
const upLimitColumn: VuelugeTorrentColumn<'max_upload_speed'> = {
  id: VuelugeColumnId.UPLIMIT,
  label: 'Up Limit',
  fields: ['max_upload_speed'],
  format: limit => vuelugeFileSizeRateFormat(limit),
};
const seedsPeersRatioColumn: VuelugeTorrentColumn<'seeds_peers_ratio'> = {
  id: VuelugeColumnId.SEEDSPEERSRATIO,
  label: 'Seeds:Peers',
  fields: ['seeds_peers_ratio'],
};
const lastTransferColumn: VuelugeTorrentColumn<'time_since_transfer'> = {
  id: VuelugeColumnId.LASTTRANSFER,
  label: 'Last Transfer',
  fields: ['time_since_transfer'],
};
const labelColumn: VuelugeTorrentColumn<'label'> = {
  id: VuelugeColumnId.LABEL,
  label: 'Label',
  fields: ['label'],
};

export const VUELUGE_TORRENT_COLUMNS = {
  queue: queueColumn,
  name: nameColumn,
  size: sizeColumn,
  progress: progressColumn,
  seeds: seedsColumn,
  peers: peersColumn,
  downSpeed: downSpeedColumn,
  upSpeed: upSpeedColumn,
  eta: etaColumn,
  ratio: ratioColumn,
  avail: availColumn,
  added: addedColumn,
  completeSeen: completeSeenColumn,
  completed: completedColumn,
  tracker: trackerColumn,
  downloadFolder: downloadFolderColumn,
  downloaded: downloadedColumn,
  uploaded: uploadedColumn,
  remaining: remainingColumn,
  downLimit: downLimitColumn,
  upLimit: upLimitColumn,
  seedsPeersRatio: seedsPeersRatioColumn,
  lastTransfer: lastTransferColumn,
  label: labelColumn,
};

export const VUELUGE_DEFAULT_SELECTED_COLUMNS: VuelugeSelectedColumns = {
  queue: true,
  name: true,
  size: true,
  progress: true,
  seeds: false,
  peers: false,
  downSpeed: true,
  upSpeed: true,
  eta: true,
  ratio: false,
  avail: false,
  added: false,
  completeSeen: false,
  completed: false,
  tracker: false,
  downloadFolder: false,
  downloaded: false,
  uploaded: false,
  remaining: false,
  downLimit: false,
  upLimit: false,
  seedsPeersRatio: false,
  lastTransfer: false,
  label: false,
};

export const VUELUGE_DEFAULT_COLUMN_ORDER: VuelugeColumnId[] = [
  VuelugeColumnId.QUEUE,
  VuelugeColumnId.NAME,
  VuelugeColumnId.SIZE,
  VuelugeColumnId.PROGRESS,
  VuelugeColumnId.SEEDS,
  VuelugeColumnId.PEERS,
  VuelugeColumnId.DOWNSPEED,
  VuelugeColumnId.UPSPEED,
  VuelugeColumnId.ETA,
  VuelugeColumnId.RATIO,
  VuelugeColumnId.AVAIL,
  VuelugeColumnId.ADDED,
  VuelugeColumnId.COMPLETESEEN,
  VuelugeColumnId.COMPLETED,
  VuelugeColumnId.TRACKER,
  VuelugeColumnId.DOWNLOADFOLDER,
  VuelugeColumnId.DOWNLOADED,
  VuelugeColumnId.UPLOADED,
  VuelugeColumnId.REMAINING,
  VuelugeColumnId.DOWNLIMIT,
  VuelugeColumnId.UPLIMIT,
  VuelugeColumnId.SEEDSPEERSRATIO,
  VuelugeColumnId.LASTTRANSFER,
  VuelugeColumnId.LABEL,
];
