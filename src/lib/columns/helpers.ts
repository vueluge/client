import {mapValues} from 'lodash';

import {VUELUGE_TORRENT_COLUMNS} from './constants';
import type {
  VuelugeSelectedColumns,
  VuelugeTorrentColumn,
  VuelugeTorrentColumnGetter,
} from './type';

export const selectedColumnFactory = (): VuelugeSelectedColumns =>
  mapValues(VUELUGE_TORRENT_COLUMNS, () => false);

export const defaultGetterFactory = (column: VuelugeTorrentColumn): VuelugeTorrentColumnGetter =>
  torrent => torrent[column.fields[0]];
