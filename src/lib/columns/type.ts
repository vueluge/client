
import type {TorrentPreview} from '~/lib/driver';

import type {VuelugeColumnId} from './enum';

export type VuelugeTorrentColumnGetter<Keys extends keyof TorrentPreview = keyof TorrentPreview, Value = TorrentPreview[Keys]> = (torrentPreview: Pick<TorrentPreview, Keys>) => Value;
export type VuelugeTorrentColumnFormatter<Value = unknown> = (value: Value) => string;
export type VuelugeTorrentColumnComparator<Value = unknown> = (a: Value, b: Value) => number;

export interface VuelugeTorrentColumn<Keys extends keyof TorrentPreview = keyof TorrentPreview, Value = TorrentPreview[Keys]> {
  id: VuelugeColumnId;
  label: string;
  fields: Keys[];
  get?: VuelugeTorrentColumnGetter<Keys, Value>;
  format?: VuelugeTorrentColumnFormatter<Value>;
  compare?: VuelugeTorrentColumnComparator<Value>;
}

export type VuelugeTorrentColumns = Record<VuelugeColumnId, VuelugeTorrentColumn>;

export type VuelugeSelectedColumns = Record<VuelugeColumnId, boolean>;
