import type {Identifiable} from '../models/indentifiable.type';
import type {VuelugeTorrentFilter} from './filter.type';

export interface VuelugeFilterGroup extends Identifiable {
  label: string;
  filters: VuelugeTorrentFilter[];
}
