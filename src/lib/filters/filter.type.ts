export type VuelugeTorrentFilter = [string, number];
export type VuelugeTorrentFilterId = VuelugeTorrentFilter[0];
