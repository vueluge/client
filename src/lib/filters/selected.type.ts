import type {VuelugeTorrentFilterId} from './filter.type';

export type VuelugeSelectedFilters = Record<string, VuelugeTorrentFilterId>;
