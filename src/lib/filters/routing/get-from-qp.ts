import type {RouteLocationNormalized} from 'vue-router';

import type {VuelugeSelectedFilters} from '~/lib/filters';
import {vuelugeRouterQueryParamsGet} from '~/lib/router';

export const vuelugeFilterRoutingGetFromQp = (route: RouteLocationNormalized, qp: string): VuelugeSelectedFilters => {
  const val = vuelugeRouterQueryParamsGet(route.query, qp);
  return val ? JSON.parse(atob(val)) : {};
};
