import type {VuelugeFiltersRoutingConfig} from './type';

export const VUELUGE_FILTER_ROUTING_CONFIG_DEFAULT: VuelugeFiltersRoutingConfig = {
  queryParam: 'filters',
};
