import {Token} from 'typedi';

import type {VuelugeFiltersRoutingConfig} from './type';

export const VUELUGE_FILTER_ROUTING_CONFIG = new Token<VuelugeFiltersRoutingConfig>('VUELUGE_FILTER_ROUTING_CONFIG');
