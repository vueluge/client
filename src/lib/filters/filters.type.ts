import type {VuelugeFilterGroup} from './group.type';

export type VuelugeTorrentFilters = Record<VuelugeFilterGroup['id'], VuelugeFilterGroup>;
