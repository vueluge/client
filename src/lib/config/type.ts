import type {VuelugeAddTorrentPreset} from '../add-torrent';

export interface VuelugeConfig {
  presets: Record<VuelugeAddTorrentPreset['name'], VuelugeAddTorrentPreset>;
  recentLocations: string[];
}
