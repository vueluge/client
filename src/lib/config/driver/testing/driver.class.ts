import {Service} from 'typedi';

import {
  MockVuelugeConfig,
  type VuelugeConfig,
} from '~/lib/config';
import type {VuelugeConfigDriverInterface} from '~/lib/config/driver';

@Service()
export class VuelugeTestingConfigDriver implements VuelugeConfigDriverInterface {
  async get (): Promise<VuelugeConfig> {
    return new MockVuelugeConfig();
  }

  async set (config: Partial<VuelugeConfig>): Promise<any> {
    return new MockVuelugeConfig();
  }
}
