import type {VuelugeConfig} from '../type';

export interface VuelugeConfigDriverInterface {
  get(): Promise<VuelugeConfig>;
  set(config: Partial<VuelugeConfig>): Promise<undefined>;
}
