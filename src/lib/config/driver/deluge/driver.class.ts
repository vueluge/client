import Container, {Service} from 'typedi';

import type {VuelugeConfig} from '~/lib/config';
import type {VuelugeConfigDriverInterface} from '~/lib/config/driver';
import {Deluge} from '~/lib/driver/deluge';

export const DELUGE_PLUGIN_CONFIG_KEY = 'vueluge';

@Service()
export class DelugeConfigDriver implements VuelugeConfigDriverInterface {
  private client: Deluge;

  constructor () {
    this.client = Container.get(Deluge);
  }

  async get (): Promise<VuelugeConfig> {
    const resp = await this.client.getConfig<Partial<VuelugeConfig>>(DELUGE_PLUGIN_CONFIG_KEY);
    return {
      presets: resp.result.presets || {},
      recentLocations: resp.result.recentLocations || [],
    };
  }

  async set (config: Partial<VuelugeConfig>): Promise<any> {
    return this.client.setConfig<VuelugeConfig>(config, DELUGE_PLUGIN_CONFIG_KEY);
  }
}
