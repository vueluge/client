import Container from 'typedi';

import {VUELUGE_CONFIG_DRIVER} from '~/lib/config/driver';

import {DelugeConfigDriver} from './driver.class';

Container.set({
  id: VUELUGE_CONFIG_DRIVER,
  type: DelugeConfigDriver,
  global: true,
});
