import {Token} from 'typedi';

import type {VuelugeConfigDriverInterface} from './type';

export const VUELUGE_CONFIG_DRIVER = new Token<VuelugeConfigDriverInterface>('VUELUGE_CONFIG_DRIVER');
