import {faker} from '@faker-js/faker/locale/en';
import {times} from 'lodash';

import type {VuelugeConfig} from './type';

export class MockVuelugeConfig implements VuelugeConfig {
  presets = {};
  recentLocations = times(faker.number.int({min: 3, max: 10}), () => faker.system.filePath());
}
