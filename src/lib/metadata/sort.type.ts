export enum VuelugeSortDirection {
  ASCENDING = 'asc',
  DESCENDING = 'desc',
}

export interface VuelugeSort {
  field: string;
  direction: VuelugeSortDirection;
}
