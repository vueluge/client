import type { VuelugePaginationRequest, VuelugePaginationResponse } from "./pagination.type";
import type { VuelugeSort } from "./sort.type";

export interface VuelugeMetadataRequest {
  sort?: VuelugeSort;
  pagination: VuelugePaginationRequest
  search: string
}

export interface VuelugeMetadataResponse {
  sort?: VuelugeSort;
  pagination: VuelugePaginationResponse
  count: number
}