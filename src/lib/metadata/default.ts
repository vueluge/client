import type {VuelugeMetadataRequest, VuelugeMetadataResponse} from './type';

export const VUELUGE_METADATA_REQUEST_DEFAULT: VuelugeMetadataRequest = {
  pagination: {
    size: 25,
    current: 1,
  },
  search: '',
};

export const VUELUGE_METADATA_RESPONSE_DEFAULT: VuelugeMetadataResponse = {
  ...VUELUGE_METADATA_REQUEST_DEFAULT,
  pagination: {
    ...VUELUGE_METADATA_REQUEST_DEFAULT.pagination,
    count: 1
  },
  count: 0
};
