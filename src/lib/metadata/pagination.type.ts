export interface VuelugePaginationRequest {
  current: number;
  size: number;
}

export interface VuelugePaginationResponse extends VuelugePaginationRequest {
  count: number;
}
