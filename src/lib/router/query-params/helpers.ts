import type {RouteLocationNormalizedLoaded} from 'vue-router';

export const vuelugeRouterQueryParamsGet = (queryParams: RouteLocationNormalizedLoaded['query'], name: string): string => {
  const qp = queryParams[name];
  return (Array.isArray(qp) ? qp[0] : qp) || '';
};

export const vuelugeRouterQueryParamsGetAll = (queryParams: RouteLocationNormalizedLoaded['query'], name: string): string[] => {
  const qp = queryParams[name];
  return Array.isArray(qp) ? qp.map((val) => val || '') : [qp || ''];
};
