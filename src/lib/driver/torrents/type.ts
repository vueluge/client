import type {
  AddTorrentPayload,
  DelugeSettings,
  Torrent,
  TorrentInfo,
  TorrentPreview,
} from '~/lib/driver';
import type {VuelugeSelectedFilters} from '~/lib/filters';
import type {VuelugeMetadataRequest} from '~/lib/metadata';

import type {VuelugeTorrentsDriverListResponse} from './response.type';

// TODO: split into multiple drivers
export interface VuelugeTorrentsDriverInterface {
  list(additionalFields?: (keyof Torrent)[], filters?: VuelugeSelectedFilters, metadata?: VuelugeMetadataRequest): Promise<VuelugeTorrentsDriverListResponse>;

  upload(files: File[]): Promise<TorrentInfo['result'][]>;
  add(payloads: AddTorrentPayload[]): Promise<TorrentPreview[]>;

  getConfig(): Promise<DelugeSettings>;
}
