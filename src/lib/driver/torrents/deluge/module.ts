import Container from 'typedi';

import '~/lib/driver/torrent/deluge/module';

import {DelugeTorrentsDriver} from './driver.class';
import {VUELUGE_TORRENTS_DRIVER} from '../token';

Container.set({
  id: VUELUGE_TORRENTS_DRIVER,
  type: DelugeTorrentsDriver,
  global: true,
});
