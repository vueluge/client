import {omit} from 'lodash';
import Container, {Service} from 'typedi';

import type {
  TorrentListResponse,
  TorrentInfo,
  DelugeSettings,
  AddTorrentPayload,
  TorrentPreview,
} from '~/lib/driver';
import {Deluge} from '~/lib/driver/deluge';
import {DelugeTorrentDriver} from '~/lib/driver/torrent/deluge';
import type {
  VuelugeTorrentsDriverInterface,
  VuelugeTorrentsDriverListResponse,
} from '~/lib/driver/torrents';
import type {VuelugeSelectedFilters} from '~/lib/filters';
import type {VuelugeMetadataRequest} from '~/lib/metadata';

import {delugeFiltersTransform} from './transforms';

@Service()
export class DelugeTorrentsDriver implements VuelugeTorrentsDriverInterface {
  private client: Deluge;
  private torrentDriver: DelugeTorrentDriver;

  constructor () {
    this.torrentDriver = Container.get(DelugeTorrentDriver);
    this.client = Container.get(Deluge);
  }

  async add (payloads: AddTorrentPayload[]): Promise<TorrentPreview[]> {
    const results = await this.client.addTorrents(payloads);
    return Promise.all(results.result.filter(([success]) => success).map(([, id]) => this.torrentDriver.getDetails(id)));
  }

  async getConfig (): Promise<DelugeSettings> {
    const resp = await this.client.getConfig();
    return resp.result;
  }

  async upload (files: File[]): Promise<TorrentInfo['result'][]> {
    const data = await this.client.upload(files);

    return data.map(({result}) => result);
  }

  async list (
    additionalFields: string[] = [],
    filters: VuelugeSelectedFilters = {},
    metadata?: VuelugeMetadataRequest,
  ): Promise<VuelugeTorrentsDriverListResponse> {
    const data = await this.client.getAllData(
      ['num_files', ...additionalFields],
      filters.state === 'All'
        ? omit(filters, 'state')
        : filters,
      metadata,
    );
    const raw: TorrentListResponse = data.raw;

    return {
      stats: raw.result.stats,
      connected: raw.result.connected,
      filters: delugeFiltersTransform(raw.result.filters),
      torrents: raw.result.torrents,
      pagination: raw.result.pagination,
      sort: raw.result.sort,
      count: raw.result.count,
    };
  }
}
