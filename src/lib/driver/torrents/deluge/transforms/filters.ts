import {mapValues} from 'lodash';

import type {TorrentFilters} from '~/lib/driver';
import type {
  VuelugeFilterGroup,
  VuelugeTorrentFilters,
} from '~/lib/filters';

const labelMap: Record<string, string> = {
  state: 'State',
  tracker_host: 'Trackers',
  owner: 'Owner',
  label: 'Label',
};

export const delugeFiltersTransform = (filters: TorrentFilters): VuelugeTorrentFilters =>
  mapValues(filters, (group, id): VuelugeFilterGroup => ({
    id,
    label: labelMap[id] || id,
    filters: group || [],
  }));
