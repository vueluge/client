import type {
  Stats,
  TorrentPreview,
} from '~/lib/driver';
import type {VuelugeTorrentFilters} from '~/lib/filters';
import type {VuelugeMetadataResponse} from '~/lib/metadata';

export interface VuelugeTorrentsDriverListResponse extends VuelugeMetadataResponse {
  stats: Stats;
  connected: boolean;
  filters: VuelugeTorrentFilters;
  torrents: TorrentPreview[];
}
