import Container from 'typedi';

import {VUELUGE_TORRENTS_DRIVER} from '../token';
import {VuelugeTestingTorrentDriver} from './driver.class';

Container.set({
  id: VUELUGE_TORRENTS_DRIVER,
  type: VuelugeTestingTorrentDriver,
  global: true,
});
