import {faker} from '@faker-js/faker/locale/en';
import {
  mapValues,
  times,
} from 'lodash';
import Container, {Service} from 'typedi';

import {
  type TorrentInfo,
  type DelugeSettings,
  type AddTorrentPayload,
  type TorrentPreview,
} from '~/lib/driver';
import {
  VuelugeTestingBackend,
  MockDelugeSettings,
  MockTorrentInfo,
  MockTorrent,
  MockStats,
} from '~/lib/driver/testing';
import type {
  VuelugeTorrentsDriverInterface,
  VuelugeTorrentsDriverListResponse,
} from '~/lib/driver/torrents';
import type {
  VuelugeFilterGroup,
  VuelugeSelectedFilters,
} from '~/lib/filters';
import type {VuelugeMetadataRequest} from '~/lib/metadata';

@Service()
export class VuelugeTestingTorrentDriver implements VuelugeTorrentsDriverInterface {
  private backend = Container.get(VuelugeTestingBackend);

  async add (payloads: AddTorrentPayload[]): Promise<TorrentPreview[]> {
    const torrents = times(payloads.length, () => new MockTorrent());
    torrents.forEach((t) => {
      this.backend.torrents.value[t.id] = t;
    });
    return torrents;
  }

  async getConfig (): Promise<DelugeSettings> {
    return new MockDelugeSettings();
  }

  async upload (files: File[]): Promise<TorrentInfo['result'][]> {
    const data = times(files.length, () => new MockTorrentInfo());

    return data.map(({result}) => result);
  }

  async list (additionalFields: string[] = [], filters: VuelugeSelectedFilters = {}, metadata?: VuelugeMetadataRequest): Promise<VuelugeTorrentsDriverListResponse> {
    if (metadata?.pagination.current) {
      this.backend.currentPage.value = metadata.pagination.current;
    }
    if (metadata?.pagination.size) {
      this.backend.pageSize.value = metadata.pagination.size;
    }
    if (metadata?.sort) {
      this.backend.sort.value = metadata.sort;
    }
    this.backend.search.value = metadata?.search || '';
    return {
      stats: new MockStats(),
      connected: faker.datatype.boolean(),
      filters: mapValues(this.backend.filters.value, (group, id): VuelugeFilterGroup => ({
        id,
        label: id,
        filters: group || [],
      })),
      torrents: this.backend.torrentList.value,
      pagination: this.backend.pagination.value,
      sort: this.backend.sort.value,
      count: Object.keys(this.backend.torrents.value).length,
    };
  }
}
