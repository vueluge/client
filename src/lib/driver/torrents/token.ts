import {Token} from 'typedi';

import type {VuelugeTorrentsDriverInterface} from './type';

export const VUELUGE_TORRENTS_DRIVER = new Token<VuelugeTorrentsDriverInterface>('VUELUGE_TORRENTS_DRIVER');
