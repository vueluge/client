import {Token} from 'typedi';

import type {DelugeConfig} from './type';

export const DELUGE_CONFIG = new Token<DelugeConfig>('DELUGE_CONFIG');
