import Container from 'typedi';

import {Deluge} from '~/lib/driver/deluge';

import {DELUGE_CONFIG} from './config';

Container.set({
  id: Deluge,
  factory: () => {
    const {
      baseUrl,
    } = Container.get(DELUGE_CONFIG);
    return new Deluge({
      baseUrl,
    });
  },
  global: true,
});
