import axios, {type AxiosResponse} from 'axios';

import type {
  HTTPRequestClient,
  RequestOptions,
  Response,
  UploadOptions,
} from './client';

const mapBody = <T>(response: AxiosResponse<T>): Response<T> => ({
  body: response.data,
  headers: <Response<T>['headers']>response.headers,
});

export const axiosClient: HTTPRequestClient = {
  async request<T>(url: string, options?: RequestOptions): Promise<Response<T>> {
    return axios
      .post<T>(url, options?.json, {
      responseType: options?.responseType,
      headers: options?.headers,
      httpAgent: options?.agent?.http,
      httpsAgent: options?.agent?.https,
      timeout: options?.timeout.request,
      signal: options?.signal,
      withCredentials: true,
    })
      .then(mapBody);
  },

  async upload<T>(url: string, options?: UploadOptions): Promise<Response<T>> {
    return axios
      .post<T>(url, options?.body, {
      httpAgent: options?.agent?.http,
      httpsAgent: options?.agent?.https,
      timeout: options?.timeout.request,
      withCredentials: true,
    })
      .then(mapBody);
  },
};
