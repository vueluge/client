import {faker} from '@faker-js/faker/locale/en';
import {keyBy, map, sortBy, times} from 'lodash';
import {Service} from 'typedi';

import {MockTorrent, MockTorrentFilters} from './factories';
import { VuelugeSortDirection } from '~/lib/metadata';
import { computed, ref } from 'vue';
import Fuse, { type IFuseOptions } from 'fuse.js';
import type { Torrent } from '../types';

const FUSE_OPTIONS: IFuseOptions<Torrent> = {
  isCaseSensitive: false,
  distance: 2,
  findAllMatches: true,
  keys: ['name']
}

@Service()
export class VuelugeTestingBackend {
  torrents = ref(keyBy(times(faker.number.int({min: 5, max: 100}), () => new MockTorrent()), 'id'));
  filters = ref(new MockTorrentFilters());
  sort = ref({
    field: 'name',
    direction: VuelugeSortDirection.ASCENDING
  });
  currentPage = ref(1)
  pageSize = ref(20)
  search = ref('')

  pagination = computed(() => ({
    current: this.currentPage.value,
    size: this.pageSize.value,
    count: Math.ceil(Object.keys(this.torrents.value).length / this.pageSize.value)
  }))

  torrentList = computed(() => {
    const torrents = this.search.value
      ? map(new Fuse(Object.values(this.torrents.value), FUSE_OPTIONS).search(this.search.value), 'item')
      : Object.values(this.torrents.value)
    const list = sortBy(torrents, this.sort.value.field)
      .slice((this.currentPage.value - 1) * this.pageSize.value, Math.min(this.currentPage.value * this.pageSize.value, torrents.length))
    return this.sort.value.direction === VuelugeSortDirection.ASCENDING
      ? list
      : list.reverse()
  })
}
