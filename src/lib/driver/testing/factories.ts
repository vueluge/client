import {faker} from '@faker-js/faker/locale/en';
import {
  keyBy,
  times,
} from 'lodash';

import {
  HostStatus,
  type AddTorrentOptions,
  type AddTorrentPayload,
  type AddTorrentResponse,
  type BooleanStatus,
  type ConfigResponse,
  type DefaultResponse,
  type DelugeSettings,
  type GetHostStatusResponse,
  type GetHostsResponse,
  type ListMethods,
  type PluginInfo,
  type PluginsListResponse,
  type Stats,
  type StringStatus,
  type Torrent,
  type TorrentContentDir,
  type TorrentContentFile,
  type TorrentFiles,
  type TorrentFilters,
  type TorrentInfo,
  type TorrentList,
  type TorrentListResponse,
  type TorrentOptions,
  type TorrentPeers,
  type TorrentPreview,
  type TorrentStatus,
  type Tracker,
  type UploadResponse,
  type VuelugeProxy,
  TorrentFSEntryType,
  TorrentState,
  type VuelugeProxyType,
} from '../types';
import { VuelugeSortDirection, type VuelugePaginationResponse } from '../../metadata';

const hostStatuses = Object.values(HostStatus);
const torrentStates = Object.values(TorrentState);

export class MockDefaultResponse implements DefaultResponse {
  /**
   * mostly usless id that increments with every request
   */
  id = faker.helpers.unique(faker.number.int);
  error = faker.helpers.arrayElement([null, faker.word.sample(5)]);
  result: unknown = null;
}

export class MockBooleanStatus extends MockDefaultResponse implements BooleanStatus {
  result = faker.datatype.boolean();
}

export class MockStringStatus extends MockDefaultResponse implements StringStatus {
  result = faker.helpers.arrayElement(hostStatuses);
}

export class MockListMethods extends MockDefaultResponse implements ListMethods {
  result = [];
}

export class MockAddTorrentResponse extends MockDefaultResponse implements AddTorrentResponse {
  result = [];
}

// {"files": ["/tmp/delugeweb-5Q9ttR/tmpL7xhth.torrent"], "success": true}
/**
 * ex -
 */
export class MockUploadResponse implements UploadResponse {
  /**
   * ex - `["/tmp/delugeweb-5Q9ttR/tmpL7xhth.torrent"]`
   */
  files = [faker.system.filePath()];
  success = faker.datatype.boolean();
}

export class MockGetHostsResponse extends MockDefaultResponse implements GetHostsResponse {
  /**
   * host id - ddf084f5f3d7945597991008949ea7b51e6b3d93
   * ip address - 127.0.0.1
   * port - 58846
   * status - "Online"
   */
  result: Array<[string, string, number, HostStatus]> = [[faker.string.uuid(), faker.internet.ipv4(), faker.number.int({min: 1, max: 99999}), faker.helpers.arrayElement(hostStatuses)]];
}

export class MockGetHostStatusResponse extends MockDefaultResponse implements GetHostStatusResponse {
  /**
   * host id - ddf084f5f3d7945597991008949ea7b51e6b3d93
   * status - "Online"
   * version - "1.3.15"
   */
  result: [string, HostStatus, string] = [faker.string.uuid(), faker.helpers.arrayElement(hostStatuses), faker.system.semver()];
}

export class MockTorrentContentFile implements TorrentContentFile {
  download = faker.datatype.boolean();
  index = faker.number.int({min: 1, max: 99});
  length = faker.number.int({min: 1000, max: 9999999});
  type: TorrentFSEntryType.FILE = TorrentFSEntryType.FILE;
  /**
   * has path when downloading folders
   */
  path = faker.system.filePath();
}

export class MockTorrentContentDir implements TorrentContentDir {
  download = true as const;
  length = faker.number.int({min: 1000, max: 9999});
  type: TorrentFSEntryType.DIRECTORY = TorrentFSEntryType.DIRECTORY;
  // TODO: implement
  contents = {};
}

export class MockTorrentInfo extends MockDefaultResponse implements TorrentInfo {
  result = {
    files_tree: {
    // TODO: implement
      contents: {},
    },
    name: faker.word.words(),
    info_hash: faker.datatype.uuid(),
    path: faker.system.filePath(),
  };
}

export class MockAddTorrentOptions implements AddTorrentOptions {
  file_priorities = [];
  add_paused = faker.datatype.boolean();
  compact_allocation = faker.datatype.boolean();
  download_location = faker.system.filePath();
  max_connections = faker.number.int({min: 1, max: 99999999});
  max_download_speed = faker.number.int({min: 1, max: 99999999});
  max_upload_slots = faker.number.int({min: 1, max: 99999999});
  max_upload_speed = faker.number.int({min: 1, max: 99999999});
  prioritize_first_last_pieces = faker.datatype.boolean();
  move_completed = faker.datatype.boolean();
  move_completed_path?: string;
  pre_allocate_storage = faker.datatype.boolean();
  sequential_download = faker.datatype.boolean();
  seed_mode = faker.datatype.boolean();
  super_seeding = faker.datatype.boolean();
}

export class MockAddTorrentPayload implements AddTorrentPayload {
  path = faker.system.filePath();
  options = new MockAddTorrentOptions();
}

/**
 * ['label', 'id']
 */
export class MockTorrentFilters implements TorrentFilters {
  state: Array<[string, number]> = [[faker.word.noun(), faker.number.int({min: 0, max: 999})]];
  tracker_host: Array<[string, number]> = [[faker.word.noun(), faker.number.int({min: 0, max: 999})]];
  label?: Array<[string, number]> = [[faker.word.noun(), faker.number.int({min: 0, max: 999})]];
}

export class MockStats implements Stats {
  upload_protocol_rate = faker.number.int({min: 1, max: 9999});
  max_upload = faker.number.int({min: 1, max: 9999});
  download_protocol_rate = faker.number.int({min: 1, max: 9999});
  download_rate = faker.number.int({min: 1, max: 9999});
  has_incoming_connections = faker.datatype.boolean();
  num_connections = faker.number.int({min: 1, max: 9999});
  max_download = faker.number.int({min: 1, max: 9999});
  upload_rate = faker.number.int({min: 1, max: 9999});
  dht_nodes = faker.number.int({min: 1, max: 9999});
  free_space = faker.number.int({min: 1, max: 9999});
  max_num_connections = faker.number.int({min: 1, max: 9999});
}

export class MockTorrentPreview implements TorrentPreview {
  last_seen_complete = faker.number.int({min: 1, max: 9999});
  completed_time = faker.number.int({min: 1, max: 9999});
  download_location = faker.system.directoryPath();
  time_since_transfer = faker.number.int({min: 1, max: 9999});
  id = faker.string.uuid();
  max_download_speed = faker.number.int({min: 1, max: 9999});
  upload_payload_rate = faker.number.int({min: 1, max: 9999});
  download_payload_rate = faker.number.int({min: 1, max: 9999});
  num_peers = faker.number.int({min: 1, max: 9999});
  ratio = faker.number.int({min: 1, max: 9999});
  total_peers = faker.number.int({min: 1, max: 9999});
  state = faker.helpers.arrayElement(torrentStates);
  max_upload_speed = faker.number.int({min: 1, max: 9999});
  eta = faker.number.int({min: 1, max: 9999});
  save_path = faker.system.filePath();
  comment = faker.word.words();
  num_files = faker.number.int({min: 1, max: 9999});
  total_size = faker.number.int({min: 1, max: 9999});
  progress = faker.number.float({min: 0, max: 100});
  time_added = faker.number.int({min: 1, max: 9999});
  tracker_host = faker.internet.ip();
  tracker = faker.word.sample();
  total_uploaded = faker.number.int({min: 1, max: 9999});
  total_done = faker.number.int({min: 1, max: 9999});
  total_remaining = faker.number.int({min: 1, max: 9999});
  total_wanted = faker.number.int({min: 1, max: 9999});
  total_seeds = faker.number.int({min: 1, max: 9999});
  seeds_peers_ratio = faker.number.int({min: 1, max: 9999});
  num_seeds = faker.number.int({min: 1, max: 9999});
  name = faker.word.words();
  is_auto_managed = faker.datatype.boolean();
  queue = faker.number.int({min: 1, max: 9999});
  distributed_copies = faker.number.int({min: 1, max: 9999});
  label = faker.word.words();
}

export class MockTorrent extends MockTorrentPreview implements Torrent {
  total_payload_upload = faker.number.int({min: 1, max: 9999});
  total_payload_download = faker.number.int({min: 1, max: 9999});
  next_announce = faker.number.int({min: 1, max: 9999});
  tracker_status = faker.word.words();
  num_pieces = faker.number.int({min: 1, max: 9999});
  piece_length = faker.number.int({min: 1, max: 9999});
  active_time = faker.number.int({min: 1, max: 9999});
  seeding_time = faker.number.int({min: 1, max: 9999});
  seed_rank = faker.number.int({min: 1, max: 9999});
  completed_time = faker.number.int({min: 1, max: 9999});
  last_seen_complete = faker.number.int({min: 1, max: 9999});
  time_since_transfer = faker.number.int({min: 1, max: 9999});
}

export class MockTorrentList implements TorrentList {
  stats = new MockStats();
  connected = faker.datatype.boolean();
  torrents = times(faker.number.int({min: 5, max: 100}), () => new MockTorrent());
  filters = new MockTorrentFilters();
  sort = {
    field: 'name',
    direction: VuelugeSortDirection.ASCENDING
  };
  pagination = {
    current: 1,
    size: 20,
    count: 5
  }
  count = 50
}

export class MockTorrentListResponse extends MockDefaultResponse implements TorrentListResponse {
  result = new MockTorrentList();
}

export class MockPluginInfo extends MockDefaultResponse implements PluginInfo {
  result = {
    Name: faker.word.words(),
    License: faker.word.words(),
    Author: faker.word.words(),
    'Home-page': faker.word.words(),
    Summary: faker.word.words(),
    Platform: faker.word.words(),
    Version: faker.word.words(),
    'Author-email': faker.word.words(),
    Description: faker.word.words(),
  };
}

export class MockPluginsListResponse extends MockDefaultResponse implements PluginsListResponse {
  result = {
    enabled_plugins: [],
    available_plugins: [],
  };
}

export class MockTracker implements Tracker {
  tier = faker.number.int({min: 1, max: 9999});
  url = faker.internet.url();
}

export class MockTorrentStatus extends MockDefaultResponse implements TorrentStatus {
  result = new MockTorrent();
}

export class MockTorrentPeers implements TorrentPeers {
  down_speed = faker.number.int({min: 1, max: 9999});
  ip = faker.internet.ip();
  up_speed = faker.number.int({min: 1, max: 9999});
  client = faker.word.sample();
  country = faker.location.country();
  progress = faker.number.int({min: 1, max: 9999});
  seed = faker.number.int({min: 1, max: 9999});
}

export class MockTorrentFiles extends MockDefaultResponse implements TorrentFiles {
  result = keyBy(times(faker.number.int({min: 1, max: 10}), () => {
    const MockClass = faker.helpers.arrayElement([MockTorrentContentDir, MockTorrentContentFile]);
    return new MockClass();
  }), () => faker.word.sample());
}

export class MockTorrentOptions implements TorrentOptions {
  max_download_speed = faker.number.int({min: 1, max: 9999});
  max_upload_speed = faker.number.int({min: 1, max: 9999});
  max_connections = faker.number.int({min: 1, max: 9999});
  max_upload_slots = faker.number.int({min: 1, max: 9999});
  prioritize_first_last = faker.datatype.boolean();
  is_auto_managed = faker.datatype.boolean();
  stop_at_ratio = faker.datatype.boolean();
  stop_ratio = faker.number.int({min: 1, max: 9999});
  remove_at_ratio = faker.datatype.boolean();
  move_completed = faker.datatype.boolean();
  move_completed_path = faker.system.filePath();
  super_seeding = faker.datatype.boolean();
}

export class MockVuelugeProxy implements VuelugeProxy {
  type: VuelugeProxyType = faker.helpers.arrayElement([0, 1, 2, 3, 4, 5]);
  hostname = faker.internet.domainName();
  username = faker.internet.userName();
  password = faker.internet.password();
  port = faker.number.int({min: 1, max: 9999});
}

// https://github.com/biwin/deluge/blob/1.3-stable/deluge/core/preferencesmanager.py
export class MockDelugeSettings implements DelugeSettings {
  /**
   * Yes, please send anonymous statistics.
   * default: false
   */
  send_info? = faker.datatype.boolean();
  /**
   * how many times info is sent? i dunno
   * default: 0
   */
  info_sent? = faker.number.int({min: 1, max: 9999});
  /**
   * default: 58846
   */
  daemon_port? = faker.number.int({min: 1, max: 9999});
  /**
   * set True if the server should allow remote connections
   * default: false
   */
  allow_remote? = faker.datatype.boolean();
  /**
   * default: /Downloads
   */
  download_location = faker.system.filePath();
  /**
   * incoming ports
   * default: [6881, 6891]
   */
  listen_ports: [number, number] = [faker.number.int({min: 1, max: 9999}), faker.number.int({min: 1, max: 9999})];
  /**
   * overrides listen_ports
   * default: true
   */
  random_port = faker.datatype.boolean();
  /**
   * default: [0, 0]
   */
  outgoing_ports: [number, number] = [faker.number.int({min: 1, max: 9999}), faker.number.int({min: 1, max: 9999})];
  /**
   * default: true
   */
  random_outgoing_ports = faker.datatype.boolean();
  /**
   * IP address to listen for BitTorrent connections
   * default: ""
   */
  listen_interface = faker.internet.ip();
  /**
   * enable torrent copy dir
   * default: false
   */
  copy_torrent_file = faker.datatype.boolean();
  /**
   * Copy of .torrent files to:
   */
  torrentfiles_location = faker.system.filePath();
  /**
   * default: False
   */
  del_copy_torrent_file = faker.datatype.boolean();
  plugins_location = faker.system.filePath();
  /**
   * Prioritize first and last pieces of torrent
   * default: False
   */
  prioritize_first_last_pieces = faker.datatype.boolean();
  /**
   * default: True
   */
  dht = faker.datatype.boolean();
  /**
   * default: True
   */
  upnp = faker.datatype.boolean();
  /**
   * default: True
   */
  natpmp = faker.datatype.boolean();
  /**
   * default: True
   */
  utpex = faker.datatype.boolean();
  /**
   * default: True
   */
  lsd = faker.datatype.boolean();
  /**
   * default: 1
   */
  enc_in_policy = faker.number.int({min: 1, max: 9999});
  /**
   * default: 1
   */
  enc_out_policy = faker.number.int({min: 1, max: 9999});
  /**
   * default: 2
   */
  enc_level = faker.number.int({min: 1, max: 9999});
  /**
   * default: True
   */
  enc_prefer_rc4 = faker.datatype.boolean();
  /**
   * default: 200
   */
  max_connections_global = faker.number.int({min: 1, max: 9999});
  /**
   * default: -1
   */
  max_upload_speed = faker.number.int({min: 1, max: 9999});
  /**
   * default: -1
   */
  max_download_speed = faker.number.int({min: 1, max: 9999});
  /**
   * default: 4
   */
  max_upload_slots_global = faker.number.int({min: 1, max: 9999});
  /**
   * default: 50
   */
  max_half_open_connections = faker.number.int({min: 1, max: 9999});
  /**
   * default: 20
   */
  max_connections_per_second = faker.number.int({min: 1, max: 9999});
  /**
   * default: True
   */
  ignore_limits_on_local_network = faker.datatype.boolean();
  /**
   * default: -1
   */
  max_connections_per_torrent = faker.number.int({min: 1, max: 9999});
  /**
   * default: -1
   */
  max_upload_slots_per_torrent = faker.number.int({min: 1, max: 9999});
  /**
   * default: -1
   */
  max_upload_speed_per_torrent = faker.number.int({min: 1, max: 9999});
  /**
   * default: -1
   */
  max_download_speed_per_torrent = faker.number.int({min: 1, max: 9999});
  enabled_plugins = [];
  // "autoadd_location": deluge.common.get_default_download_dir(),
  /**
   * default: False
   */
  autoadd_enable = faker.datatype.boolean();
  /**
   * default: False
   */
  add_paused = faker.datatype.boolean();
  max_active_seeding = faker.number.int({min: 1, max: 9999});
  max_active_downloading = faker.number.int({min: 1, max: 9999});
  max_active_limit = faker.number.int({min: 1, max: 9999});
  /**
   * default: False
   */
  dont_count_slow_torrents = faker.datatype.boolean();
  /**
   * default: False
   */
  queue_new_to_top = faker.datatype.boolean();
  /**
   * default: False
   */
  stop_seed_at_ratio = faker.datatype.boolean();
  /**
   * default: False
   */
  remove_seed_at_ratio = faker.datatype.boolean();
  /**
   * default: 2
   */
  stop_seed_ratio = faker.number.int({min: 1, max: 9999});
  /**
   * default: 2
   */
  share_ratio_limit = faker.number.int({min: 1, max: 9999});
  /**
   * default: 7
   */
  seed_time_ratio_limit = faker.number.int({min: 1, max: 9999});
  /**
   * default: 180
   */
  seed_time_limit = faker.number.int({min: 1, max: 9999});
  /**
   * default: True
   */
  auto_managed = faker.datatype.boolean();
  /**
   * default: False
   */
  move_completed = faker.datatype.boolean();
  move_completed_path = faker.system.filePath();
  /**
   * default: True
   */
  new_release_check = faker.datatype.boolean();
  /**
   * Rate limit IP overhead
   * default: true
   */
  rate_limit_ip_overhead = faker.datatype.boolean();
  /**
   * default: '/usr/share/GeoIP/GeoIP.dat'
   */
  geoip_db_location = faker.system.filePath();
  /**
   * default: 512
   */
  cache_size = faker.number.int({min: 1, max: 9999});
  /**
   * default: 60
   */
  cache_expiry = faker.number.int({min: 1, max: 9999});
}

export class MockConfigResponse extends MockDefaultResponse implements ConfigResponse {
  result = new MockDelugeSettings();
}
