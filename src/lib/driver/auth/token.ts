import {Token} from 'typedi';

import type {VuelugeAuthDriverInterface} from './type';

export const VUELUGE_AUTH_DRIVER = new Token<VuelugeAuthDriverInterface>('VUELUGE_AUTH_DRIVER');
