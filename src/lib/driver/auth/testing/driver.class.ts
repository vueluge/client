import {Service} from 'typedi';

import type {VuelugeAuthDriverInterface} from '~/lib/driver/auth';

@Service()
export class VuelugeTestingAuthDriver implements VuelugeAuthDriverInterface {
  async login (password: string): Promise<void> {
    return undefined;
  }

  async logout (): Promise<void> {
    return undefined;
  }

  async check (): Promise<void> {
    return undefined;
  }
}
