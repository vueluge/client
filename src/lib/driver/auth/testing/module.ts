import Container from 'typedi';

import {VuelugeTestingAuthDriver} from './driver.class';
import {VUELUGE_AUTH_DRIVER} from '../token';

Container.set({
  id: VUELUGE_AUTH_DRIVER,
  type: VuelugeTestingAuthDriver,
  global: true,
});
