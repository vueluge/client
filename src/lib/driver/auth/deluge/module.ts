import Container from 'typedi';

import {VUELUGE_AUTH_DRIVER} from '../token';
import {DelugeAuthDriver} from './driver.class';

Container.set({
  id: VUELUGE_AUTH_DRIVER,
  type: DelugeAuthDriver,
  global: true,
});
