import Container, {Service} from 'typedi';

import type {VuelugeAuthDriverInterface} from '~/lib/driver/auth';
import {Deluge} from '~/lib/driver/deluge';

@Service()
export class DelugeAuthDriver implements VuelugeAuthDriverInterface {
  private client: Deluge;

  constructor () {
    this.client = Container.get(Deluge);
  }

  async login (password: string): Promise<void> {
    if (!await this.client.login(password)) {
      throw new Error('Incorrect password');
    }
  }

  async logout (): Promise<void> {
    if (!await this.client.logout()) {
      throw new Error('Logout failed');
    }
  }

  async check (): Promise<void> {
    if (!await this.client.checkSession()) {
      throw new Error('Not authenticated');
    }
  }
}
