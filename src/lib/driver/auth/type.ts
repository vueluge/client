export interface VuelugeAuthDriverInterface {
  login(password: string): Promise<void>;
  logout(): Promise<void>;
  check(): Promise<void>;
}
