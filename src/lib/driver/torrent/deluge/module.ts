import Container from 'typedi';

import {VUELUGE_TORRENT_DRIVER} from '../token';
import {DelugeTorrentDriver} from './driver.class';

Container.set({
  id: VUELUGE_TORRENT_DRIVER,
  type: DelugeTorrentDriver,
  global: true,
});
