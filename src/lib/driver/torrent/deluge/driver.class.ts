import Container, {Service} from 'typedi';

import type {
  TorrentContentDir,
  TorrentContentFile,
  TorrentPeers,
  TorrentOptions,
  TorrentPreview,
  Torrent,
  TorrentStatus,
} from '~/lib/driver';
import {Deluge} from '~/lib/driver/deluge';
import type {VuelugeTorrentDriverInterface} from '~/lib/driver/torrent';

@Service()
export class DelugeTorrentDriver implements VuelugeTorrentDriverInterface {
  private client: Deluge;

  constructor () {
    this.client = Container.get(Deluge);
  }

  async getDetails (torrentId: Torrent['id']): Promise<Torrent> {
    return (await this.client.getTorrentStatus(torrentId)).result;
  }

  async getFiles (torrentId: TorrentPreview['id']): Promise<Record<string, TorrentContentDir | TorrentContentFile>> {
    return (await this.client.getTorrentFiles(torrentId)).result;
  }

  getPeers (torrentId: TorrentPreview['id']): Promise<TorrentPeers[]> {
    throw new Error('Method not implemented.');
  }

  async getOptions (torrentId: TorrentPreview['id']): Promise<TorrentOptions> {
    return (await this.client.getTorrentStatus<{result: TorrentOptions & Torrent} & TorrentStatus>(
      torrentId,
      [
        'max_download_speed',
        'max_upload_speed',
        'max_connections',
        'max_upload_slots',
        'prioritize_first_last',
        'is_auto_managed',
        'stop_at_ratio',
        'stop_ratio',
        'remove_at_ratio',
        'move_completed',
        'move_completed_path',
        'super_seeding',
      ],
    )).result;
  }

  async setOptions (torrentId: TorrentPreview['id'], options: Partial<TorrentOptions>): Promise<TorrentOptions> {
    return (await this.client.setTorrentOptions(
      torrentId,
      options,
    )).result;
  }

  async pause (torrentId: TorrentPreview['id']): Promise<void> {
    const {
      error,
    } = await this.client.pauseTorrent(torrentId);

    if (error) {
      throw new Error(error);
    }
  }

  async resume (torrentId: TorrentPreview['id']): Promise<void> {
    const {
      error,
    } = await this.client.resumeTorrent(torrentId);

    if (error) {
      throw new Error(error);
    }
  }

  async remove (torrentId: TorrentPreview['id']): Promise<void> {
    const {
      error,
    } = await this.client.removeTorrent(torrentId);

    if (error) {
      throw new Error(error);
    }
  }

  async recheck (torrentId: TorrentPreview['id']): Promise<void> {
    const {
      error,
    } = await this.client.verifyTorrent(torrentId);

    if (error) {
      throw new Error(error);
    }
  }

  async move (torrentId: TorrentPreview['id'], destination: string): Promise<void> {
    const {
      error,
    } = await this.client.moveStorage([torrentId], destination);

    if (error) {
      throw new Error(error);
    }
  }
}
