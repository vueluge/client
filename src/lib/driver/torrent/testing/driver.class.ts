import Container, {Service} from 'typedi';

import {
  type TorrentContentDir,
  type TorrentContentFile,
  type TorrentPeers,
  type TorrentOptions,
  type TorrentPreview,
  type Torrent,
} from '~/lib/driver';
import { MockTorrent, MockTorrentFiles, VuelugeTestingBackend } from '~/lib/driver/testing';
import type {VuelugeTorrentDriverInterface} from '~/lib/driver/torrent';

@Service()
export class VuelugeTestingTorrentDriver implements VuelugeTorrentDriverInterface {
  private backend = Container.get(VuelugeTestingBackend);

  async getDetails (torrentId: Torrent['id']): Promise<Torrent> {
    return this.backend.torrents.value[torrentId]
  }

  async getFiles (torrentId: TorrentPreview['id']): Promise<Record<string, TorrentContentDir | TorrentContentFile>> {
    return new MockTorrentFiles().result;
  }

  getPeers (torrentId: TorrentPreview['id']): Promise<TorrentPeers[]> {
    throw new Error('Method not implemented.');
  }

  getOptions (torrentId: TorrentPreview['id']): Promise<TorrentOptions> {
    throw new Error('Method not implemented.');
  }

  setOptions (torrentId: TorrentPreview['id']): Promise<TorrentOptions> {
    throw new Error('Method not implemented.');
  }

  pause (torrentId: TorrentPreview['id']): Promise<void> {
    throw new Error('Method not implemented.');
  }

  resume (torrentId: TorrentPreview['id']): Promise<void> {
    throw new Error('Method not implemented.');
  }

  remove (torrentId: TorrentPreview['id']): Promise<void> {
    throw new Error('Method not implemented.');
  }

  recheck (torrentId: TorrentPreview['id']): Promise<void> {
    throw new Error('Method not implemented.');
  }

  move (torrentId: TorrentPreview['id'], destination: string): Promise<void> {
    throw new Error('Method not implemented.');
  }
}
