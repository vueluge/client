import Container from 'typedi';

import {VuelugeTestingTorrentDriver} from './driver.class';
import {VUELUGE_TORRENT_DRIVER} from '../token';

Container.set({
  id: VUELUGE_TORRENT_DRIVER,
  type: VuelugeTestingTorrentDriver,
  global: true,
});
