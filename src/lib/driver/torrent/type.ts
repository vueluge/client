import type {
  Torrent,
  TorrentFiles,
  TorrentOptions,
  TorrentPeers,
  TorrentPreview,
} from '~/lib/driver';

export interface VuelugeTorrentDriverInterface<T extends Torrent = Torrent> {
  getDetails(torrentId: Torrent['id'], additionalFields?: string[]): Promise<T>;
  getFiles(torrentId: TorrentPreview['id']): Promise<TorrentFiles['result']>;
  getPeers(torrentId: TorrentPreview['id']): Promise<TorrentPeers[]>;
  getOptions(torrentId: TorrentPreview['id']): Promise<TorrentOptions>;
  setOptions(torrentId: TorrentPreview['id'], options: Partial<TorrentOptions>): Promise<TorrentOptions>;
  pause(torrentId: TorrentPreview['id']): Promise<void>;
  resume(torrentId: TorrentPreview['id']): Promise<void>;
  remove(torrentId: TorrentPreview['id']): Promise<void>;
  recheck(torrentId: TorrentPreview['id']): Promise<void>;
  move(torrentId: TorrentPreview['id'], destination: string): Promise<void>;
}
