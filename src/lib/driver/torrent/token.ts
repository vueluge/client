import {Token} from 'typedi';

import type {VuelugeTorrentDriverInterface} from './type';

export const VUELUGE_TORRENT_DRIVER = new Token<VuelugeTorrentDriverInterface>('VUELUGE_TORRENT_DRIVER');
