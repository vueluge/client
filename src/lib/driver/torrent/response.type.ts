import type {
  Stats,
  TorrentPreview,
} from '~/lib/driver';
import type {VuelugeTorrentFilters} from '~/lib/filters';

export interface VuelugeTorrentDriverListResponse {
  stats: Stats;
  connected: boolean;
  filters: VuelugeTorrentFilters;
  torrents: TorrentPreview[];
}
