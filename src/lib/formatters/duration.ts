import prettyMilliseconds, {type Options} from 'pretty-ms';

export const vuelugeDurationFormat = (
  milliseconds: number,
  options?: Options,
): string =>
  milliseconds > 0
    ? prettyMilliseconds(milliseconds, options)
    : '-';
