export const vuelugeDateFormatter = (seconds: number) =>
  seconds > 0 ? new Date(seconds * 1e3).toLocaleString() : '-';
