import {vuelugeFileSizeFormat} from './file-size';

export const vuelugeFileSizeRateFormat = (bytes: number, si = false, dp = 1) => {
  if (bytes === 0) {
    return '-';
  }

  const size = vuelugeFileSizeFormat(bytes, si, dp);

  return `${size}/s`;
};
