import type {Identifiable} from '../models';

export const arrayToDict = <T extends Identifiable = Identifiable>(array: T[]): Record<T['id'], T> =>
  array.reduce(
    (dict, val) => {
      dict[val.id as T['id']] = val;
      return dict;
    },
    {} as Record<T['id'], T>,
  );
