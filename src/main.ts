import 'reflect-metadata';
import 'vuetify/styles'; // Global CSS has to be imported
import {createHead} from '@vueuse/head';
import {createPinia} from 'pinia';
import Container from 'typedi';
import {createApp} from 'vue';

import vuetify from '~/plugins/vuetify';

import App from './App.vue';
import {importDrivers} from './drivers/drivers';
import router from './router';

import './assets/main.css';
import '~/lib/filters/routing/module';

Container.set(AbortController, new AbortController());

importDrivers().then(() => {
  const app = createApp(App);
  const head = createHead();

  app.use(head);
  app.use(createPinia());
  app.use(router);
  app.use(vuetify);

  app.mount('#app');
});
