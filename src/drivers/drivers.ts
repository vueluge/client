import Container from 'typedi';

import {DELUGE_CONFIG} from '~/lib/driver/deluge';

export const importDrivers = () => {
  const platform = import.meta.env.VITE_PLATFORM || 'testing';
  let driverImport: Promise<unknown>;
  switch (platform) {
    case 'deluge':
      driverImport = import('~/drivers/deluge.module');
      Container.set(DELUGE_CONFIG, {
        baseUrl: import.meta.env.DELUGE_BASE_URL || window.location.origin,
      });
      break;

    case 'testing':
    default:
      driverImport = import('~/drivers/testing.module');
      break;
  }
  return driverImport;
};
