import '@ctrl/deluge/dist/src/axios.js';
import {Deluge} from '@ctrl/deluge';
import {Agent} from 'https';

const client = new Deluge({
  baseUrl: 'https://nas:8112/',
  password: '1q2W3024$',
  agent: {
    https: new Agent({
      rejectUnauthorized: false,
    }),
  },
});
(async () => console.log((await client.listTorrents()).result))();
