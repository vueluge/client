import vue from '@vitejs/plugin-vue';
import {
  fileURLToPath,
  URL,
} from 'node:url';
import copy from 'rollup-plugin-copy';
import {
  defineConfig,
  loadEnv,
} from 'vite';
import vuetify from 'vite-plugin-vuetify';

// https://vitejs.dev/config/
export default defineConfig(({mode}) => {
  const {VITE_DELUGE_PLUGIN_DIR} = loadEnv(mode, process.cwd());

  return {
    build: {
      sourcemap: 'inline',
    },
    plugins: [
      vue(),
      vuetify({autoImport: true}),
      copy({
        targets: [
          {
            src: 'dist/*',
            dest: `${VITE_DELUGE_PLUGIN_DIR}/.python-eggs/Vueluge-0.1.0-py3.11.egg-tmp/vueluge/data/client`,
          },
        ],
        hook: 'writeBundle',
      }),
    ],
    resolve: {
      alias: {
        '~': fileURLToPath(new URL('./src', import.meta.url)),
        '@': fileURLToPath(new URL('./src/components', import.meta.url)),
      },
    },
    define: {
      global: 'window',
    },
    base: process.env.SSG ? '/client/' : '/vueluge/',
    envPrefix: [
      'VITE_',
      'DELUGE_',
    ],
  };
});
