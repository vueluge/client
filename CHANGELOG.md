# Changelog

## [v0.1.0](https://gitlab.com/vueluge/client/-/releases/v0.1.0)

### Features

- **driver/testing**: implement backend
- **lib**: revert cadence type change
- cancel requests when resetting cadence
- allow for empty sort
- update testing drivers to new interfaces
- use server data table
- use `vueluge.update_ui` method
- add metadata
- upgrade vuetify
- add torrent options footer pane
- **store**: add set torrent options
- **driver**: add torrent options
- add display breakpoints to torrent status
- add generics to torrent store
- add torrent context menu
- remove route keys from template
- **store**: type return of torrent entities store composable
- **driver**: add torrent actions
- **driver**: add `moveStorage` to deluge client
- **driver**: add paused torrent state to enum
- import reference torrent driver
- split apart torrent driver
- set title with `@vueuse/head`
- add support for switching driver platforms
- add platform driver modules
- add testing drivers
- add model factories
- preserve query params as well
- preserve home path upon login redirect
- add query param helpers and composable
- drop use of `NormalizedTorrent`
- set stats in title
- add system bar
- move filters store hydration to routing layer
- close preset dialog after save
- add remove preset button
- add filters query param
- format `eta` column
- add torrent status
- add login errors
- add logout button to app bar
- truncate progress to precision three
- add login view and auto check auth on startup
- add torrent with file priorities
- add presets
- add plugin config driver and store
- split add torrent options into separate component
- seed add torrent options from server config
- add torrent options

### Bug Fixes

- type errors
- cannot remove search query
- limit form field bad listener pass through
- wrong driver reference
- many build errors
- update presets deletes everything
- `progress` column displaying more than 2 decimal places
- `eta` formatter
- **driver**: `All` filter returns no torrents
- login redirect
